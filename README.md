# SANER 2021 - Binary level toolchain provenance identification with graph neural networks

This repository contained the code and dataset to reproduce experiments of the paper accepted at the 28th IEEE International Conference on Software Analysis, Evolution and Reengineering (SANER).

Because of a new storage policy, this repository was cleared of its content in January 2025.
However, by March 2025, the content will be uploaded to Zenodo as a permanent record, and this repository will be updated with a link to that record.
We apologize for any inconvenience the delay in migration may cause.

## Abstract
We consider the problem of recovering the compiling chain used to generate a given stripped binary code. We present a Graph Neural Network framework at the binary level to solve this problem, with the idea to take into account the shallow semantics provided by the binary code's structured control flow graph (CFG).

We introduce a Graph Neural Network, called Site Neural Network (SNN), dedicated to this problem. To attain scalability at the binary level, feature extraction is simplified by forgetting almost everything in a CFG except transfer control instructions and performing a parametric graph reduction.

Our experiments show that our method recovers the compiler family with a very high F1-Score of 0.9950 while the optimization level is recovered with a moderately high F1-Score of 0.7517. On the compiler version prediction task, the F1-Score is about 0.8167 excluding the clang family.

A comparison with a previous work demonstrates the accuracy and performance of this framework.

### Acknowledgments
This work is supported by (i) a public grant overseen by the French National Research Agency (ANR) as part of the "Investissements d'Avenir" French PIA project "Lorraine Université d'Excellence", reference ANR-15-IDEX-04-LUE, and (ii) has received funding from the European Union’s Horizon 2020 research and innovation programme under grant agreement No 830927 (Concordia).

Experiments presented in this paper were carried out using the Grid'5000 experimental testbed, being developed under the INRIA ALADDIN development action with support from CNRS, RENATER and several Universities as well as other funding bodies (see https://www.grid5000.fr).

### Citation
If you use this repository or datasets for your project please cite:
```
@inproceedings{benoit2021snn,
  title={Binary level toolchain provenance identification with graph neural networks},
  author={Benoit T., Marion J-Y., Bardin S.},
  booktitle={Proceedings of the 28th IEEE International Conference on Software Analysis, Evolution and Reengineering (SANER)},
  year={2021}
}
```
